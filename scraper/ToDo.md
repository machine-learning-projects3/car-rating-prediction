- Add command line arguments to collect links before crawling [DONE]
    - else, crawl directly links saved in files

- Connection to database to save scraped cars [DONE]
    - Set up a PostgreSQL db

- Function to create table in database if not exists [DONE]

- Function to create database if not exists [DONE]

- Save already scraped pages [DONE]

- Create function in scrapers: [DONE]
    -  To add fields [DONE]
        - Postal --> Région, Département, prix carte grise en préfecture
    - Check validity of data [DONE]
        - In case of redirect, all values are null except url, manufacturer (given by default to scraper)

- On scrape la version du model et pas le model, voir scraping du titre de l'annonce pour le modèle [DONE]

- Modifier le bdd en conséquence [DONE]

- Fonction pour: [DONE]
    - Passer un unicode.unicode sur tous les string
    - Passer un lower() sur tous les strings

- Voir comment gérer les model/version
    - Les model sont souvent egaux aux versions, voir comment gérer ça:
        - enlever de model ce qu'il y a dans version ?
        - Scraper une liste de tous les modèles de chaque constructeur pour trouver des correspondances ?

