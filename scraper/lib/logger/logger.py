import logging
import os

from datetime import datetime
from jsonformatter import JsonFormatter


def get_logger(logger_name: str) -> logging.Logger:

    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    STRING_FORMAT = """{
    "Asctime":         "asctime",
    "Levelname":       "levelname",    
    "Message":         "message"
}"""

    #  Handler stdout
    formatter = JsonFormatter(STRING_FORMAT)
    c_handler = logging.StreamHandler()
    c_handler.setLevel(logging.INFO)
    c_handler.setFormatter(formatter)

    # Handler file
    # We create the file
    os.makedirs(
        os.path.dirname("./logs/logs_{datetime.today().date()}.json"), exist_ok=True
    )
    f_handler = logging.FileHandler(
        f"./logs/logs_{datetime.today().date()}.json", mode="a"
    )
    f_handler.setLevel(logging.INFO)
    f_handler.setFormatter(formatter)

    # Add handlers to the logger
    logger.addHandler(c_handler)
    logger.addHandler(f_handler)
    return logger