import os
import requests
import threading
import time
import json
from requests.api import head
import unidecode
import queue
import random
import pandas as pd

from fake_useragent import UserAgent
from collections import namedtuple
from bs4 import BeautifulSoup


class paru_vendu:
    def __init__(self, collect_link, queue_exchange_bdd, logger) -> None:
        self._url_site = "https://www.paruvendu.fr/"
        self._url_manufacturers = "https://www.paruvendu.fr/a/voiture/marques"

        # UserAgent object to use random User_Agent header
        self._user_agent = UserAgent()
        # Collect link before scrap
        self._collect_link = collect_link
        #  Total of new links found
        self._links_cmp = 0
        # Logging
        self.logger = logger
        # Queue for links to cars pages
        self._queue_links = queue.Queue(maxsize=0)
        # Queue for scraped infos (dict info)
        self._queue_cars_info = queue.Queue(maxsize=0)
        # Queue to save in file car pages scraped
        self.queue_save_links_car = queue.Queue(maxsize=0)
        # Path to config in order to cache
        self._config_path = "./lib/scrapers/config/paru_vendu"
        # File for relation between departement and region
        self._path_relation_dep_reg = os.path.join(
            self._config_path, "relation_dep_reg.json"
        )
        # Cache for found link to cars
        self._path_scraped_cars = os.path.join(
            self._config_path, "manufacturers_scraped"
        )
        # Cache for already scraped links
        self._path_scraped_index = os.path.join(
            self._config_path, "index_pages_scraped"
        )
        # Already scraped car links
        self.path_already_scraped_page = os.path.join(
            self._path_scraped_cars, "scraped_links.json"
        )
        # Queue to send in in order to save in database
        self.queue_exchange_bdd = queue_exchange_bdd

    def check_folder_exists(self, path_to_folder):
        if not os.path.exists(path_to_folder):
            os.makedirs(path_to_folder)
            self.logger.info(f"--> Folder {path_to_folder} as been created")
        else:
            self.logger.info(f"--> Folder {path_to_folder} already exists")

    def check_file_exists(self, path_file):
        if not os.path.exists(path_file):
            self.logger.info(f"--> Created file {path_file}")
            os.mknod(path_file)
        else:
            self.logger.info(f"--> File {path_file} already exists")

    def get_page(self, url: str, wait=True):
        if wait:
            time.sleep(random.randint(1, 5))
            
        # Header for request
        header = {"User-Agent": f"{self._user_agent.random}"}
        page = requests.get(url, headers=header)

        # Check for redirection
        for hist in page.history:
            if hist.status_code == 301:
                self.logger.warning(f"--> Redirection while getting page {url}")
                return None
        return page.text

    def scrap_manufacturers_list(self) -> list:
        """
        - Function to get the list of car's manufacturer and the link to their section on paru_vendu.fr
        - Returns:
            - list: list(tuple("name_manufacturer", link_to_manufacturer_cars))
        """
        url_ = self._url_manufacturers
        page_ = self.get_page(url_, False)

        if page_ != None:
            soup = BeautifulSoup(page_, features="html.parser")

            # Manufacturers are sub divided alphabetically, we therefore find each subsection
            manufacturers_list = []
            for letter in "abcdefghijklmnopqrstuvwxyz".upper():
                # find_all() method returns a list so we add this list to our global manufacturers list
                manufacturers_list += soup.find_all(
                    "li", {"class": f"marqmod_nom class-{letter}"}
                )

            # For each manufacturer, we want the name of the manufacturer and the link to the section
            manufacturers_list = list(
                map(
                    lambda x: (
                        x.text.replace(" ", "-").lower(),
                        self._url_site[:-1] + x.find("a")["href"],
                    ),
                    manufacturers_list,
                )
            )

            return manufacturers_list

    def get_all_links_page(self, url_page):

        page = self.get_page(url_page)

        if page != None:
            soup_page = BeautifulSoup(page, features="html.parser")

            # Get all cars links on the page
            container_announces = soup_page.find("div", {"class": "ergov3-main"})

            if container_announces != None:
                try:
                    cars_announces = container_announces.find_all(
                        "div", {"class": "lazyload_bloc ergov3-annonce"}
                    )

                    cars_links_page = []
                    for announce in cars_announces:
                        if announce != None:
                            links = announce.find_all("a", href=True, title=True)
                            for link in links:
                                if "https" in link["href"]:
                                    cars_links_page.append(link["href"])
                                    break

                    return cars_links_page
                except:
                    return []
            return []

    def find_next_page_url(self, base_url) -> str:
        page = self.get_page(base_url, False)

        if page != None:
            soup = BeautifulSoup(page, features="html.parser")

            # We take all values elements of a class that can be previous or next page
            links_to_pages = soup.find_all("div", {"class", "v2pv15-pagsuiv flol"})

            # We only take the link to next page by filtering on the text
            link_to_next_page = next(
                (x for x in links_to_pages if "suivante" in x.text), None
            )
            # We return the link to next page
            if link_to_next_page:
                link_to_next_page = link_to_next_page.find("a")["href"]
                return link_to_next_page

            return None

    def save_links_file(self, list_links, path_to_file):
        # Create a temporary file
        with open(path_to_file + ".tmp", "w") as fo:
            json.dump(list_links, fo, indent=4)

        # Renaming is atomic, so if there's an outage the permanent file will
        # be either the old version or new version, but never a partial file.
        os.rename(path_to_file + ".tmp", path_to_file)

    def get_all_manufacturer_cars_links(self, tuple_manufacturer):

        name_manufacturer = tuple_manufacturer[0]
        base_url = tuple_manufacturer[1]
        starting_point = base_url

        # Check if save folder exists
        path_save_links = os.path.join(self._path_scraped_index, name_manufacturer)
        self.check_folder_exists(path_save_links)

        # Check save file exists
        path_file_save_links = os.path.join(path_save_links, "links.json")
        self.check_file_exists(path_file_save_links)

        # Import json of manufacturer links
        try:
            with open(path_file_save_links, "r") as file_save:
                manufacturer_dict_links = json.load(file_save)
        except:
            self.logger.exception(f"Error during load of file {path_file_save_links}")
            # If error during reading save file, empty list is given
            manufacturer_dict_links = {"manufacturer": name_manufacturer, "links": []}

        # Extracting list of links
        all_manufacturer_links = manufacturer_dict_links["links"]

        self.logger.info(f"--> {name_manufacturer} --> Starting collecting links")

        while True:
            # Variable to signal if file need to be saved (new links found)
            need_save = False

            # Take all the announce link on the given url
            links = self.get_all_links_page(base_url)

            for link in links:
                # Add link to knowned link if unknown
                if link not in all_manufacturer_links:
                    self._links_cmp += 1
                    need_save = True
                    all_manufacturer_links.append(link)

                    # Update on the number of new links collected
                    self.logger.info(f"--> Collected {self._links_cmp} cars links")

            #  Save file if new link
            if need_save:
                try:
                    manufacturer_dict_links["links"] = all_manufacturer_links
                    self.save_links_file(manufacturer_dict_links, path_file_save_links)
                except:
                    self.logger.exception(
                        f"Error occured during file save for {name_manufacturer}"
                    )

            # Find link to next page
            next_page_href = self.find_next_page_url(base_url)
            if next_page_href == None:
                self.logger.info(
                    f"--> {name_manufacturer} --> No next page, stopping links collection"
                )
                return
            else:
                base_url = starting_point + next_page_href

    def scrap_car_page(self, tuple_link):
        self.logger.info(f"--> Scraping {tuple_link[1]}")
        start = time.perf_counter()
        dict_info = {}

        # First infos, directly given by the tuple
        dict_info["manufacturer"] = unidecode.unidecode(tuple_link[0].lower())
        dict_info["url"] = tuple_link[1]

        # Parse page in Beautiful soup
        page = self.get_page(tuple_link[1])

        if page != None:
            # Main page
            soup = BeautifulSoup(page, features="html.parser")

            # Removing links polluting the page
            for match in soup.findAll("a"):
                match.unwrap()

            # Container
            infos = soup.find("div", {"class": "cotea16-mes"})

            # Sub container with more infos
            additional_infos = soup.find_all("li", {"class": "nologo"})

            # Price
            try:
                price = infos.find("li", {"class": "px"}).find("span").text.strip()
                price = price.replace("€", "").replace(" ", "").strip()
                dict_info["price_euro"] = int(price)
            except:
                dict_info["price_euro"] = None

            # Model
            try:
                model = soup.find(
                    "h1", {"class": "auto2012_dettophead1txt1"}
                ).text.strip()
                dict_info["model"] = unidecode.unidecode(model.lower())
            except:
                dict_info["model"] = None

            # Version
            try:
                version = infos.find("li", {"class": "vers"}).find("span").text.strip()
                # Unidecode decode accent to ascii
                dict_info["version"] = unidecode.unidecode(version.lower())
            except:
                dict_info["version"] = None

            # Year
            try:
                year = infos.find("li", {"class": "ann"}).find("span").text.strip()
                full_year = year.split()
                for elem in full_year:
                    if elem.isnumeric():
                        year = elem
                        break
                    else:
                        year = None

                dict_info["year"] = year.lower()
            except:
                dict_info["year"] = None

            # Kilometers
            try:
                kil = infos.find("li", {"class": "kil"}).find("span").text.strip()
                kil = kil.replace("km", "").replace(" ", "").strip()
                dict_info["kilometers"] = int(kil)
            except:
                dict_info["kilometers"] = None

            # Fuel type
            try:
                fuel_type = infos.find("li", {"class": "en"}).find("span").text.strip()
                dict_info["fuel_type"] = unidecode.unidecode(fuel_type.lower())
            except:
                dict_info["fuel_type"] = None

            # Emissions
            try:
                emissions = (
                    infos.find("li", {"class": "emiss"}).find("span").text.strip()
                )
                emissions = emissions.replace("g/km", "").strip()
                dict_info["co2_emissions(g/kg)"] = int(emissions)
            except:
                dict_info["co2_emissions(g/kg)"] = None

            # Consumption
            try:
                consumption = (
                    infos.find("li", {"class": "cons"}).find("span").text.strip()
                )
                consumption = consumption.split("litres")[0].strip()
                dict_info["fuel_consumption(liters/km)"] = float(consumption)
            except:
                dict_info["fuel_consumption(liters/km)"] = None

            # Transmission
            try:
                transmission = (
                    infos.find("li", {"class": "vit"}).find("span").text.strip().lower()
                )
                dict_info["transmission_type"] = unidecode.unidecode(transmission)
            except:
                dict_info["transmission_type"] = None

            # Number doors
            try:
                n_doors = consumption = (
                    infos.find("li", {"class": "carro"}).find("span").text.strip()
                )
                n_doors = n_doors.split()[0].strip()
                dict_info["n_doors"] = int(n_doors)
            except:
                dict_info["n_doors"] = None

            # Tax horsepower
            try:
                tax_horsepower = (
                    infos.find("li", {"class": "puiss"})
                    .find("span")
                    .text.strip()
                    .lower()
                )
                tax_horsepower = tax_horsepower.replace("cv", "").strip()
                dict_info["tax_horsepower"] = int(tax_horsepower)
            except:
                dict_info["tax_horsepower"] = None

            #  Number of seats
            try:
                n_seats = (
                    infos.find("li", {"class": "por"}).find("span").text.strip().lower()
                )
                n_seats = n_seats.replace("places", "").strip()
                dict_info["n_seats"] = n_seats
            except:
                dict_info["n_seats"] = None

            # Postal Code
            try:
                seller_location = soup.find_all("div", {"class": "opt19_listlinks"})
                postal = None
                for elem in seller_location:
                    for sub in elem.text.split():
                        if sub.replace("(", "").replace(")", "").isnumeric():
                            postal = sub.replace("(", "").replace(")", "")
                            break
                dict_info["postal_code"] = postal
            except:
                dict_info["postal_code"] = None

            # Region
            try:
                dict_info["region"] = unidecode.unidecode(
                    self.get_region_from_departement(
                        dict_info["postal_code"][0:2]
                    ).lower()
                )
            except:
                dict_info["region"] = None

            # Type seller
            try:
                type_seller = soup.find("p", {"class": "txtpresentation-vendeur"})
                if "Vendeur particulier" in type_seller.text:
                    dict_info["type_seller"] = "private person"
                else:
                    dict_info["type_seller"] = "professional"
            except:
                dict_info["type_seller"] = None

            # Real horse_power
            try:
                horse_power = None
                for elem in additional_infos:
                    if "Puissance réelle" in elem.text:
                        horse_power = elem.find("span").text.strip()
                dict_info["horse_power"] = int(horse_power)
            except:
                dict_info["horse_power"] = None

            # Color
            try:
                color = None
                for elem in additional_infos:
                    if "Couleur" in elem.text:
                        color = elem.find("span").text.strip()
                dict_info["color"] = unidecode.unidecode(color.lower())
            except:
                dict_info["color"] = None

            # Vehicle condition
            try:
                vehicle_condition = None
                for elem in additional_infos:
                    if "Etat du véhicule" in elem.text:
                        vehicle_condition = elem.find("span").text.strip()
                dict_info["vehicle_condition"] = unidecode.unidecode(vehicle_condition)
            except:
                dict_info["vehicle_condition"] = None

            # Final result
            end = time.perf_counter()
            self.logger.info(f"--> Page {tuple_link[1]} scraped in {end-start} second")

            # If result correct and not full of null values
            if self.check_validity_infos(dict_info, ["manufacturer", "url"]):
                return dict_info
            else:
                self.logger.error(f"--> All null values for page {tuple_link[1]}")
                return None

        else:
            return None

    def scrap_all_cars(self):
        time.sleep(3)
        self.logger.info("--> Starting scraping")

        # Assert config file with already scraped link exists
        self.check_folder_exists(os.path.join(self._path_scraped_cars))
        self.check_file_exists(
            os.path.join(self._path_scraped_cars, "scraped_links.json")
        )

        # Read file where already scraped links are saved
        with open(self.path_already_scraped_page, "r") as file:
            already_scraped = json.load(file)

        # All links are put in queue, we get them
        while not self._queue_links.empty():
            # Element in queue ara tuple(manufacturer_name, link_to_page)
            tuple_link = self._queue_links.get()

            # Check if link not already been scraped
            if tuple_link[1] not in already_scraped:
                dict_car = self.scrap_car_page(tuple_link)

                if dict_car != None:
                    # Send to save in database
                    self.queue_exchange_bdd.put(dict_car)

                    # Add scraped link to list
                    already_scraped.append(tuple_link[1])
                    self.save_links_file(
                        already_scraped, self.path_already_scraped_page
                    )
            else:
                self.logger.info(f"--> Already scraped link {tuple_link[1]}")

    def load_link_file(self):
        total_links = 0

        # We walk through the folder of link
        for (dirpath, dirnames, _) in os.walk(self._path_scraped_index):
            # Directories of each manufacturer
            for dirname in dirnames:
                path = os.path.join(dirpath, dirname, "links.json")

                with open(path, "r") as json_file:
                    links_dict = json.load(json_file)
                    manufacturer = links_dict["manufacturer"]

                links = links_dict["links"]
                for link in links:
                    tuple_link = (manufacturer, link)
                    # Put in queue each tuple, queue linked to scrapers threads
                    self._queue_links.put(tuple_link)
                total_links += len(links)
        self.logger.info(f"--> Total links in memory: {total_links}")

    def check_validity_infos(self, dict_car: dict, keys_no_check: list) -> bool:
        # If all values are None, then we have been redirected
        unvalid_dict = all(
            [
                value == None
                for key, value in dict_car.items()
                if key not in keys_no_check
            ]
        )
        if unvalid_dict:
            return False
        return True

    def save_car_links_scraped(self):
        links_scraped = []

        while True:
            if not self.queue_save_links_car.empty():
                link = self.queue_save_links_car.get()
                links_scraped.append(link)
                self.save_links_file(links_scraped, self.path_already_scraped_page)

    def get_region_from_departement(self, postal_code: str):
        with open(self._path_relation_dep_reg, "r") as file:
            relation = json.load(file)
        return relation.get(postal_code)

    def collect_cars_info(self) -> dict:
        # List of manufacturers
        manufacturers_list = self.scrap_manufacturers_list()

        # For each manufacturer, we collect all the link of its cars
        # Those links will be put in our queue for links which contains tuple (name_manufacturer, link_to_car)
        if self._collect_link:
            threads_links = []
            for manufacturer_tuple in manufacturers_list:
                thread = threading.Thread(
                    target=self.get_all_manufacturer_cars_links,
                    args=[manufacturer_tuple],
                )
                threads_links.append(thread)
                thread.start()
            [t.join() for t in threads_links]

        # Launch 2 threads in order to scrap pages on links
        self.load_link_file()
        threads_scrap = []

        # We create thread to scrap info on links
        for i in range(2):
            thread = threading.Thread(target=self.scrap_all_cars)
            threads_scrap.append(thread)
            thread.start()

        # Thread to save links of cars already scraped
        thread_save = threading.Thread(target=self.save_car_links_scraped)
        thread_save.start()

        [thread.join() for thread in threads_scrap]
        thread_save.join()
