import psycopg2
import json
import queue
import threading

from psycopg2 import sql


class database_operator:
    def __init__(self, logger, path_config_bdd):
        self.logger = logger
        # Path to config to connect datbase
        self.path_config = path_config_bdd
        # Config after reading from file in path
        self.config = self.get_config()

        # Connection to database
        self.conn = self.connect_database()
        # Assert database exist
        self.assert_db_exists()
        # Assert table exists
        self.assert_table_exists()

        # Queue to receive element to put in database
        self.queue_exchange = queue.Queue(maxsize=0)

        # Start listening on queue exchange to receive car to save in db
        thread_listen = threading.Thread(target=self.listen_queue_exchange, args=[])
        thread_listen.start()

    def get_config(self):
        # Read config
        try:
            with open(self.path_config, "r") as config_file:
                config = json.load(config_file)
            self.logger.info(
                f"--> Correctly loaded config for database {config['database']} on {config['host']}"
            )
            return config
        except:
            self.logger.exception("!! Could not read config file given on path: {}")
            return None

    def connect_database(self):
        # Parameters
        HOST = self.config["host"]
        USER = self.config["user"]
        PASSWORD = self.config["pass"]
        DATABASE = self.config["database"]

        # Open connection
        try:
            conn = psycopg2.connect(
                "host=%s dbname=%s user=%s password=%s"
                % (HOST, DATABASE, USER, PASSWORD)
            )
            self.logger.info(f"--> Successfully connected to {DATABASE} on {HOST}")
            return conn
        except:
            self.logger.exception(
                "!! Impossible to connect to data base, check given config: "
            )
            return None

    def assert_db_exists(self):
        name_db = self.config["database"]
        try:
            with self.conn.cursor() as cur:
                # Auto commit necessary to exec command
                self.conn.autocommit = True

                sqlCreateDatabase = "CREATE DATABASE " + name_db + ";"

                cur.execute(sqlCreateDatabase)
                cur.commit()
                self.logger.info(f"--> Succesfully created database {name_db}")
        except psycopg2.errors.DuplicateDatabase:
            self.logger.info(f"--> Database {name_db} already exists")
        except:
            self.logger.exception(
                f"!! Could not assert database {name_db} exists | Error:"
            )

    def assert_table_exists(self):
        # Command to create table in database
        table_name = self.config["table"]
        command = f"""CREATE TABLE IF NOT EXISTS {table_name}(
                id SERIAL PRIMARY KEY,
                manufacturer VARCHAR(100),
                url VARCHAR(150) UNIQUE,
                price_euro INTEGER ,
                model VARCHAR(150),
                version VARCHAR(150),
                year_car INTEGER,
                kilometers INTEGER,
                fuel_type VARCHAR(100),
                co2_emissions_g_km FLOAT(10),
                fuel_consumption_liters_km FLOAT(5),
                transmission_type VARCHAR(100),
                n_doors INTEGER CHECK(n_doors > 0),
                tax_horsepower INTEGER,
                n_seats INTEGER CHECK(n_seats > 0),
                postal_code CHAR(5),
                region VARCHAR(100),
                type_seller VARCHAR(100),
                horse_power INTEGER,
                color VARCHAR(100),
                vehicle_condition VARCHAR(100)
        );"""

        try:
            with self.conn.cursor() as cur:
                # Auto commit necessary to exec command
                self.conn.autocommit = True

                cur.execute(command)
                self.conn.commit()
                self.logger.info(f"--> Succesfully created table {table_name}")
        except:
            self.logger.exception(
                f"Could not assert table {table_name} exists | Error:"
            )

    def delete_table(self):
        name_table = self.config["table"]
        SQL = f"DROP TABLE {name_table};"
        try:
            with self.conn.cursor() as cursor:
                self.conn.autocommit = True
                cursor.execute(SQL)
            self.logger.info(f"--> Successfully deleted {name_table}")
        except:
            self.logger.exception(f"!! Could not delete table {name_table} | Error: ")

    def reset_table(self):
        self.logger.info(f"--> Reseting table {self.config['table']}")
        self.delete_table()
        self.assert_table_exists()

    def close_connection(self):
        try:
            self.conn.close()
            self.logger.info(
                f"--> Correctly closed connection to database {self.config['database']} on {self.config['host']}"
            )
        except:
            self.logger.exception(
                f"Failed to close connection database {self.config['database']} on {self.config['host']}"
            )

    def insert_in_table(self, dict_car):
        # Open a cursor to send SQL commands
        with self.conn.cursor() as cur:

            # Insert values in table
            try:
                SQL = (
                    "INSERT INTO paru_vendu(url, manufacturer, price_euro, model, version, year_car, kilometers,"
                    "fuel_type, co2_emissions_g_km, fuel_consumption_liters_km, transmission_type, "
                    "n_doors, tax_horsepower, n_seats, postal_code,region, type_seller, horse_power, color, vehicle_condition)"
                    " VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);"
                )

                data = (
                    dict_car["url"],
                    dict_car["manufacturer"],
                    dict_car["price_euro"],
                    dict_car["model"],
                    dict_car["version"],
                    dict_car["year"],
                    dict_car["kilometers"],
                    dict_car["fuel_type"],
                    dict_car["co2_emissions(g/kg)"],
                    dict_car["fuel_consumption(liters/km)"],
                    dict_car["transmission_type"],
                    dict_car["n_doors"],
                    dict_car["tax_horsepower"],
                    dict_car["n_seats"],
                    dict_car["postal_code"],
                    dict_car["region"],
                    dict_car["type_seller"],
                    dict_car["horse_power"],
                    dict_car["color"],
                    dict_car["vehicle_condition"],
                )

                cur.execute(SQL, data)

                # Commit changes in database
                self.conn.commit()
                self.logger.info(
                    f"--> Correctly saved in database {self.config['database']}"
                )

            except:
                self.logger.exception(
                    f"!! Could not commit changes in database {self.config['database']}: "
                )

    def listen_queue_exchange(self):
        self.logger.info(
            f"--> Started listening on queue database exchnage for {self.config['database']}:{self.config['table']}"
        )
        while True:
            if not self.queue_exchange.empty():
                dict_car = self.queue_exchange.get()
                self.insert_in_table(dict_car)

    def get_data_database(self):
        cur = self.conn.cursor()
        # Execute a SQL SELECT command
        sql = "SELECT * FROM paru_vendu"
        cur.execute(sql)

        # Fetch data line by line
        raw = cur.fetchone()
        print(raw)

        # Close self.connection
        self.conn.close()

    def get_queue_exchange(self):
        return self.queue_exchange
