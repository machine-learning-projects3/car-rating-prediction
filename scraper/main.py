import lib.logger.logger as logs_lib
import lib.scrapers.paru_vendu as paru_vendu
import lib.database.data_base as data_base_lib
import argparse


def arg_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("--reset", dest="reset", action="store_true")
    parser.add_argument("--links", dest="links", action="store_true")
    parser.set_defaults(reset=False, links=False)

    args = parser.parse_args()
    return args


if __name__ == "__main__":
    ARGS = arg_parser()
    print(ARGS)

    # Initialisation of logger
    logger = logs_lib.get_logger("scraper_logger")

    """Init database table paru_vendu"""
    try:
        paru_vendu_bdd = data_base_lib.database_operator(
            logger=logger, path_config_bdd="./lib/database/config/paru_vendu.json"
        )

        # Queue to receive elements to save in database
        queue_paru_vendu = paru_vendu_bdd.get_queue_exchange()

        # Reset database if requested
        if ARGS.reset:
            paru_vendu_bdd.reset_table()

    except:
        raise SystemExit("Impossible to create db, exit")

    # Scrapers - paru_vendu
    paru_vendu = paru_vendu.paru_vendu(ARGS.links, queue_paru_vendu, logger)
    paru_vendu.collect_cars_info()
