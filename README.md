# Car Rating Prediction

This project aims to create a machine learning model able to predict the future price of cars based on multiple criterias.

## Modules
- Web scraper made in Golang to obtain best possible performances with reliable language
	- https://golang.org/
- PostgreSQL database server to store our data in different tables
	- https://www.postgresql.org/
- Machine learning model (To debate)
	- Pandas
	- Sklearn
	- Numpy
	- Seaborn/Matplotlib/Plotly
